const app = require('./app');
const config = require('./config');
const db = require('./db');

db().then(info => {
    app.listen(config.PORT, () => console.log(`Example app listening on port ${config.PORT}!`));
}).catch((err) => {
    console.error(`Unable to connect DB${err}`);
    process.exit(1);
});

