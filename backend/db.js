const mongoose = require('mongoose');
const config = require('./config');

module.exports = () => {
    return new Promise((resolve,reject) => {
        mongoose.Promise = global.Promise;
        mongoose.get('debug',true);

        let db = mongoose.connection;
        db.on('error', error => reject(error));
        db.once('open', () => {
            resolve(mongoose.connection);
        });
        mongoose.connect(config.MONGO_URL, {useNewUrlParser: true});
    })
};
