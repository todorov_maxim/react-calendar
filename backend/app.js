const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
const Shemes = require('./mongo-scheme');


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// getEvents
app.get('/getEvents', (req, res) => {
    Shemes.Date.find({}, (err, events) => {
        res.send(events);
    });
});

// addEvent
app.post('/addEvent', (req, res) => {
    let id = new mongoose.Types.ObjectId();
    let date = new Shemes.Date({
        _id: id,
        ...req.body
    });
    date.save((err) => {
        if (err) throw err;
        console.log('Event successfully saved.');
        return res.json(date);
    });
});

// remove Event
app.post('/removeEvent', (req, res)=>{
    const id = req.body.id;
    Shemes.Date.findOneAndDelete( {_id: id}, (err, date) =>{
        if (err) throw err;
        return res.json({
            "success": true
        });
    });
});

// edit Event
app.post('/editEvent', (req, res)=>{
    const id = req.body.id;
    const message = req.body.message;
    Shemes.Date.findOneAndUpdate({_id: id}, { $set: { message: message } }, { new: true }, (err, doc) => {
        if (err) throw err;
        return res.json({
            "success": true
        });
    });
});


module.exports = app;