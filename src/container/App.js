import {connect} from 'react-redux'
import App from '../components/App'
import {addEvent,getEvents,removeEvent, updateEvent} from '../action/events'


export default connect(
    state => ({
        events: state
    }),
    dispatch => ({
        addEvent: (data) => dispatch(addEvent(data)),
        removeEvent:(id) => dispatch(removeEvent(id)),
        getEvents: () => dispatch(getEvents()),
        updateEvent: (id,message) => dispatch(updateEvent(id,message))
    })
)(App)