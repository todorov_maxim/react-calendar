const domain = 'http://localhost:7777';

export const post = (url, params) => {
    return fetch(domain + url, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(params),
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.success !== undefined && data.success === false)
                throw data.message || 'Success false';
            return data
        })
};

export const get = (url) => {
    return fetch(domain + url, {
        method: 'GET',
        headers: {'Content-Type': 'application/json'},
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.success !== undefined && data.success === false)
                throw data.message || 'Success false';
            return data
        })
};
