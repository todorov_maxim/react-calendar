import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux'
import thunk from 'redux-thunk'
import {Provider} from 'react-redux'
import Events from './reducers/events'
import './index.css';
import App from './container/App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const Store = createStore(Events, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={Store}><App /></Provider>, document.getElementById('root'));

