import React, {Component} from 'react';
import 'antd/dist/antd.css'
import {Badge, Button, Calendar, Input, Radio} from 'antd';
import {isEqual} from 'date-fns'

const colors = {
    birthDay: 'error',
    work: 'processing',
    date: 'success',
    family: 'warning'
};

const addBlockStyles = {
    marginBottom: 50,
    paddingTop: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:'center',
    flexDirection: 'column',
    paddingLeft:50
};

const getDate = (date) => {
    return new Date(date).setHours(0,0,0,0)
};

class App extends Component {

    state = {
        activeDate: null,
        category: '',
        message: '',
        onEdit: false,
        editId: null,
        editMessage: ''
    };

    componentWillMount() {
        return this.props.getEvents()
    };

    getListData = (value) => {
        const events = this.props.events;
        let currentEvents;
        let listData;
        if(events)
            currentEvents = events.filter(event => isEqual(getDate(new Date(event.date)), getDate(new Date(value._d))));
        if (currentEvents)
            listData = currentEvents.map(item => ({type: colors[item.category], content: item.message, id: item._id}));
        return listData || [];
    };

    dateCellRender = (value) => {
        const listData = this.getListData(value);
        return (
            <ul className="events">
                {
                    listData.map(item => (
                        <li key={item.id} id={item.id} onClick={this.onChangeEvent}>
                            <Badge status={item.type} text={item.content}/>
                        </li>
                    ))
                }
            </ul>
        );
    };

    onChangeEvent = (e) => {
        return this.setState({onEdit: true, editId: e.currentTarget.id})
    };

    deleteEvent = () => {
        return this.props.removeEvent(this.state.editId)
    };

    editEvent = () => {
        return this.props.updateEvent(this.state.editId, this.state.editMessage)
    };

    onSelect = (date) => {
        return this.setState({activeDate: date._d})
    };
    addEvent = () => {
        return this.props.addEvent(
            {
                date: this.state.activeDate || new Date(new Date().setHours(0, 0, 0, 0)),
                category: this.state.category,
                message: this.state.message
            })
    };
    onCheck = (e) => {
        return this.setState({category: e.target.value});
    };
    onChange = (e) => {
        if (e.target.name === 'edit')
            return this.setState({editMessage: e.target.value});
        return this.setState({message: e.target.value});
    };
    render() {
        const RadioGroup = Radio.Group;
        return (
            <div className="App" style={{display: 'flex', flexDirection: 'row'}}>
                <Calendar dateCellRender={this.dateCellRender} monthCellRender={this.monthCellRender}
                          onSelect={this.onSelect} style={{width: 900}}/>
                <div id='functional' style={{dispay: 'flex', flexDirection: 'column'}}>
                    <div id='add' style={addBlockStyles}>
                <Button type="primary" onClick={this.addEvent} disabled={this.state.category === '' || this.state.message === ''}>Add Event</Button>
                <RadioGroup>
                    <Radio value='birthDay' onChange={this.onCheck}>Birth Day</Radio>
                    <Radio value='work' onChange={this.onCheck}>Work</Radio>
                    <Radio value='date' onChange={this.onCheck}>Date </Radio>
                    <Radio value='family' onChange={this.onCheck}>Family</Radio>
                </RadioGroup>
                {this.state.category === 'birthDay' &&
                <Input name='birthDay' placeholder='Message for Birth Day' value={this.state.message}
                       onChange={this.onChange}/>}
                {this.state.category === 'work' &&
                <Input name='work' placeholder='Message for Work' value={this.state.message} onChange={this.onChange}/>}
                {this.state.category === 'date' &&
                <Input name='date' placeholder='Message for Date' value={this.state.message} onChange={this.onChange}/>}
                {this.state.category === 'family' &&
                <Input name='family' placeholder='Message for Family' value={this.state.message} onChange={this.onChange}/>}
                    </div>

                    {this.state.onEdit && <div>
                        <Button type="dashed" onClick={this.editEvent} disabled={this.state.editMessage===''}>Edit Event</Button>
                        <Input name='edit' placeholder='Edit your message' value={this.state.editMessage}
                               onChange={this.onChange}/>
                        <Button type="danger" onClick={this.deleteEvent}>Delete Event</Button>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default App;